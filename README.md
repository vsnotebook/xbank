## Biz-SIP示例项目xbank
### 1. 示例项目xbank简介
xbank是一家商业银行，面向个人客户和公司客户，其中个人客户业务包括存款、贷款、缴费等业务；银行业务渠道除了传统柜面以外，还有网上银行、手机银行、ATM、POS等，最近准备上一个针对银行合作伙伴的基于OPENAPI网关的开放平台渠道。
本示例项目是以个人客户中的存款查询和缴费业务为例子，渠道采用OPENAPI开放接口，后台系统对接个人客户存款系统和个人客户信息系统，第三方对接缴费平台，来演示如何打造基于Biz-SIP中间件的银行业务中台。
### 2. xbank示例项目模板介绍
xbank项目分为以下模块：

- xbank-source：适配层模块，存放外部适配接入的适配层实现。
   - xbank-openapi-source：OPENAPI接入的适配层子模块
- xbank-app：应用层模块，存放应用层的服务实现。
- xbank-sink：领域层模块，存放按领域划分的领域服务实现。
   - xbank-account-sink：账户域子模块
   - xbank-customer-sink：客户域子模块
   - xbank-payment1-sink：缴费域子模块（存储转发）
   - xbank-payment1-sink：缴费域子模块（交易补偿）
- xbank-infrastructure：基础设施层模块，存放对数据库、内容存储、HSM等基础设施的访问能力实现。
   - xbank-account-db：账户数据库访问子模块
   - xbank-customre-db：客户数据库访问子模块
- xbank-client：接口模块，存放各层之间的服务接口，以及相关数据DTO。
   - xbank-account-sink-client：账户域服务接口子模块
   - xbank-customer-sink-client：客户域服务接口子模块
   - xbank-app-client：应用服务接口子模块