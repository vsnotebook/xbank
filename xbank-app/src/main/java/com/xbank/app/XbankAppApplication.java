package com.xbank.app;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

/**
 * @author 史正烨
 */
@SpringBootApplication
@ComponentScan(basePackages={"com.xbank.app","com.bizmda.bizsip.app","cn.hutool.extra.spring"})
@Import(cn.hutool.extra.spring.SpringUtil.class)
public class XbankAppApplication {
    public static void main(String[] args) {
        SpringApplication.run(XbankAppApplication.class, args);
    }
}