package com.xbank.client.app.api;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizMessage;
import com.xbank.client.app.dto.CustomerAndAccountList;
import com.xbank.infrastructure.db.account.domain.Account;
import com.xbank.infrastructure.db.customer.domain.Customer;

import java.util.List;

public interface PersonalAppInterface {
    public CustomerAndAccountList getCustomerAndAccountList(String customerId);
    public List<Account> getAccountListByCustomerId(String customerId);
    public Customer getCustomer(String customerId);
    public BizMessage send2Payment1(Object message) throws BizException;
    public BizMessage send2Payment2(String tranMode, String tranCode, Object message) throws BizException;
    public Customer getCustomerAndSaf2Payment2(String tranMode, String customerId) throws BizException;
    public Account payout(String accountId,long amount);
    public void payoutForward(String tranMode,String accountId,long amount) throws BizException;
    public void payoutForwardCompensate(JSONObject jsonObject) throws BizException;
    public void payoutBackward(String tranMode,String accountId,long amount) throws BizException;
    public void payoutBackwardCompensate(JSONObject jsonObject) throws BizException;
}
