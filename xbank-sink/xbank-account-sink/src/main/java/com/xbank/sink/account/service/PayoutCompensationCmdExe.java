package com.xbank.sink.account.service;

import com.bizmda.bizsip.sink.cmdexe.AbstractBeanCmdExe;
import com.xbank.infrastructure.db.account.domain.Account;
import com.xbank.infrastructure.db.account.service.AccountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
public class PayoutCompensationCmdExe extends AbstractBeanCmdExe {
    @Autowired
    private AccountService accountService;

    @Transactional
    public Account payoutCompensation(String accountId, long amount) {
        Account account = this.accountService.getById(accountId);
        account.setBalance(account.getBalance() + amount);
        this.accountService.updateById(account);
        log.info("payoutCompensation({},{}),余额:{}",accountId,amount,account.getBalance());
        return account;
    }
}
