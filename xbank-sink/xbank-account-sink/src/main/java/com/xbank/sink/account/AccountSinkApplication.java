package com.xbank.sink.account;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

/**
 * @author 史正烨
 */
@Slf4j
@SpringBootApplication
@MapperScan("com.xbank.infrastructure.db.account.mapper")
@ComponentScan(basePackages={"cn.hutool.extra.spring","com.xbank"})
@Import(cn.hutool.extra.spring.SpringUtil.class)
public class AccountSinkApplication {
    public static void main(String[] args) {
        SpringApplication.run(AccountSinkApplication.class, args);
    }
}