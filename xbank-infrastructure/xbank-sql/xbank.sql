-- ----------------------------
-- Table structure for account
-- ----------------------------
DROP TABLE IF EXISTS `account`;
CREATE TABLE `account` (
  `account_id` varchar(10) NOT NULL COMMENT '账号',
  `customer_id` varchar(10) DEFAULT NULL COMMENT '客户号',
  `balance` bigint DEFAULT NULL COMMENT '余额',
  PRIMARY KEY (`account_id`)
) COMMENT='账户表';

-- ----------------------------
-- Records of account
-- ----------------------------
BEGIN;
INSERT INTO `account` VALUES ('0001', '001', 100);
INSERT INTO `account` VALUES ('0002', '001', 200);
INSERT INTO `account` VALUES ('0003', '002', 300);
INSERT INTO `account` VALUES ('0004', '003', 400);
COMMIT;

-- ----------------------------
-- Table structure for customer
-- ----------------------------
DROP TABLE IF EXISTS `customer`;
CREATE TABLE `customer` (
  `customer_id` varchar(10) NOT NULL COMMENT '客户号',
  `customer_name` varchar(20) DEFAULT NULL COMMENT '姓名',
  `age` smallint DEFAULT NULL COMMENT '年龄',
  `sex` char(1) DEFAULT NULL COMMENT '性别',
  PRIMARY KEY (`customer_id`)
) COMMENT='客户信息表';

-- ----------------------------
-- Records of customer
-- ----------------------------
BEGIN;
INSERT INTO `customer` VALUES ('001', '张三', 30, '1');
INSERT INTO `customer` VALUES ('002', '李四', 40, '0');
COMMIT;

-- ----------------------------
-- Table structure for payment_tran_log
-- ----------------------------
DROP TABLE IF EXISTS `payment_tran_log`;
CREATE TABLE `payment_tran_log` (
  `id` int NOT NULL AUTO_INCREMENT,
  `tran_datetime` datetime DEFAULT NULL COMMENT '交易时间',
  `tran_code` varchar(10) DEFAULT NULL COMMENT '交易码',
  `account_id` varchar(10) DEFAULT NULL COMMENT '账号',
  `tran_amount` int DEFAULT NULL COMMENT '交易金额',
  `retry_count` int DEFAULT NULL COMMENT '重试次数',
  `tran_status` varchar(20) DEFAULT NULL COMMENT '交易状态',
  PRIMARY KEY (`id`)
) COMMENT='缴费支付流水表';

