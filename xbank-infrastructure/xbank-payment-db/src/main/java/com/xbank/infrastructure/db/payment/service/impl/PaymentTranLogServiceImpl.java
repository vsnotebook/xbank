package com.xbank.infrastructure.db.payment.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xbank.infrastructure.db.payment.domain.PaymentTranLog;
import com.xbank.infrastructure.db.payment.service.PaymentTranLogService;
import com.xbank.infrastructure.db.payment.mapper.PaymentTranLogMapper;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class PaymentTranLogServiceImpl extends ServiceImpl<PaymentTranLogMapper, PaymentTranLog>
    implements PaymentTranLogService{

}




