package com.xbank.infrastructure.db.customer.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xbank.infrastructure.db.customer.domain.Customer;
import com.xbank.infrastructure.db.customer.service.CustomerService;
import com.xbank.infrastructure.db.customer.mapper.CustomerMapper;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class CustomerServiceImpl extends ServiceImpl<CustomerMapper, Customer>
    implements CustomerService{

}




